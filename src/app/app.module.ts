import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule } from '@angular/forms';
 import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { VideoComponent } from './ov-video.component';
import { UserVideoComponent } from './user-video.component';
import { HTTP } from '@ionic-native/http/ngx';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
const config: SocketIoConfig = { url: 'https://axsacra.convolink.ai'};
@NgModule({
  declarations: [AppComponent,VideoComponent,UserVideoComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),
     AppRoutingModule,FormsModule,ReactiveFormsModule,SocketIoModule.forRoot(config),CommonModule],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AndroidPermissions,
    NativeStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
