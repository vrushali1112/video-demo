import { Component, HostListener } from "@angular/core";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { Socket } from "ngx-socket-io";
import { HTTP } from "@ionic-native/http/ngx";
import {
  OpenVidu,
  Publisher,
  Session,
  StreamEvent,
  StreamManager,
  Subscriber,
} from "openvidu-browser";
import { Router } from "@angular/router";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { stringify } from 'querystring';
declare var cordova;

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  public postData = {
    username: "",
    password: "",
  };
  isUser = false;
  private apiServer = "api";
  join = null;
  otherUser = null;
  refid = null;
  role = "PUBLISHER";
  users = [];
  token = null;
  publisherView = null;
  publishVideo = true;
  publishAudio = true;
  currentUser = null;
  audioMute = false;
  videoMute = false;
  // OpenVidu objects
  OV: OpenVidu;
  session: Session;
  publisher: StreamManager; // Local
  subscribers: StreamManager[] = []; // Remotes

  // Join form
  mySessionId: string;
  myUserName: string;

  //android permission array
  ANDROID_PERMISSIONS = [
    this.androidPermissions.PERMISSION.CAMERA,
    this.androidPermissions.PERMISSION.RECORD_AUDIO,
    this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS,
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private http: HTTP,
    private androidPermissions: AndroidPermissions,
    private socket: Socket,
    private nativeStorage: NativeStorage
  ) {
    this.initializeApp();
    this.isUserLogin();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  validateInputs() {
    let username = this.postData.username.trim();
    let password = this.postData.password.trim();
    return (
      this.postData.username &&
      this.postData.password &&
      username.length > 0 &&
      password.length > 0
    );
  }

  isUserLogin() {
    // if (localStorage.getItem("userId")) {
    //   this.isUser = true;
    // }
    this.nativeStorage.getItem("userId").then(
      (data) => {
        console.log("in nativeStorage===>>>", data);
        var mydata =  JSON.parse(JSON.stringify(data))
        console.log("mydata==>>",mydata)
        this.currentUser = mydata.userid
        if(this.currentUser){
          this.isUser= true
        }
      }
    ).catch((data)=>{
      console.log("in nativeStorage error==>>>>",data)
    });
  }

  loginAction() {
    if (this.validateInputs) {
      var appkey =
        "bc903462ded7955626dfce5a44c78d6e51ee6f416a3f34fbc3c1bf6cec656c32";
      let headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        responseType: "text",
      };
      var obj = {
        username: this.postData.username,
        password: this.postData.password,
        appkey: appkey,
      };
      this.http
        .post("https://taliccb.convolink.ai/talic/routeorgapi", obj, headers)
        .then((response) => {
          var res = response.data;
          var resData = res.substring(res.indexOf("{"));
          var userDetails = JSON.parse(resData);
          console.log("userDetailsss==>>>", userDetails);
          // localStorage.setItem("userId", userDetails.userid);
          this.nativeStorage.setItem("userId", userDetails.userid).then(() => {
            console.log("data stored successfully");
            this.isUser = true
            // window.location.reload();
          }).catch((error)=>{
            console.log("in error==>>>")
          })
        })
        .catch((response) => {
          console.log("errorrr==>>>>>", response);
        });
    }
  }

  destroySocket() {
    if (this.socket) this.socket.disconnect();
  }

  //checking  android video audio permission
  private checkAndroidPermissions(): Promise<any> {
    console.log("in  checking permission android==>>");
    return new Promise((resolve, reject) => {
      this.platform.ready().then(() => {
        console.log("in platform.ready==>>>>>>");
        this.androidPermissions
          .requestPermissions(this.ANDROID_PERMISSIONS)
          .then(() => {
            this.androidPermissions
              .checkPermission(this.androidPermissions.PERMISSION.CAMERA)
              .then((camera) => {
                this.androidPermissions
                  .checkPermission(
                    this.androidPermissions.PERMISSION.RECORD_AUDIO
                  )
                  .then((audio) => {
                    this.androidPermissions
                      .checkPermission(
                        this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS
                      )
                      .then((modifyAudio) => {
                        if (
                          camera.hasPermission &&
                          audio.hasPermission &&
                          modifyAudio.hasPermission
                        ) {
                          resolve();
                        } else {
                          reject(
                            new Error(
                              "Permissions denied: " +
                                "\n" +
                                " CAMERA = " +
                                camera.hasPermission +
                                "\n" +
                                " AUDIO = " +
                                audio.hasPermission +
                                "\n" +
                                " AUDIO_SETTINGS = " +
                                modifyAudio.hasPermission
                            )
                          );
                        }
                      })
                      .catch((err) => {
                        console.error(
                          "Checking permission " +
                            this.androidPermissions.PERMISSION
                              .MODIFY_AUDIO_SETTINGS +
                            " failed"
                        );
                        reject(err);
                      });
                  })
                  .catch((err) => {
                    console.error(
                      "Checking permission " +
                        this.androidPermissions.PERMISSION.RECORD_AUDIO +
                        " failed"
                    );
                    reject(err);
                  });
              })
              .catch((err) => {
                console.error(
                  "Checking permission " +
                    this.androidPermissions.PERMISSION.CAMERA +
                    " failed"
                );
                reject(err);
              });
          })
          .catch((err) => console.error("Error requesting permissions: ", err));
      });
    });
  }
  makeCall(obj) {
    this.nativeStorage.getItem("userId").then(
      (data) => {
        console.log("in nativeStorage===>>>", JSON,stringify(data));
          var mydata =  JSON.parse(JSON.stringify(data))
          console.log("mydata==>>",mydata)
          this.currentUser = mydata.userid
          if(this.currentUser){
            this.socketConnect(obj);
          }
      }
    ).catch((data)=>{
      console.log("in nativeStorage error==>>>>",data)
    });
  }

  socketConnect(callData) {
    this.socket.connect();
    // console.log("storage==>>>", localStorage.getItem("userId"));
    // this.currentUser = localStorage.getItem("userId");
    callData = {
      agentid: null,
    };
    console.log("this.currentuser====>>>",this.currentUser)
    var usrtype = "agent";
    this.socket.emit("registerUser", {
      emailid: this.currentUser,
      utype: usrtype,
      otheruser: this.otherUser,
      agentkey: callData.agentid,
      refid: this.refid,
    });

    this.socket.on("serverError", (error) => {
      this.destroySocket();
    });
    this.socket.on("userConnected", (data) => {
      console.log("user connected/*******", data);
      this.otherUser = data.emailid;
      if (!this.join)
        this.socket.emit("getToken", {
          emailid: this.currentUser,
          utype: usrtype,
          refid: this.refid,
          role: this.role,
        });
    });

    this.socket.on("newUserConnected", (data) => {
      console.log("new user connected/******", data);
      if (!this.users[data.emailid])
        this.users[this.users.length] = data.emailid;
    });

    this.socket.on("tokenGenerated", (data) => {
      this.OV = new OpenVidu();
      this.session = this.OV.initSession();
      this.session.on("streamCreated", (event: StreamEvent) => {
        const subscriber: Subscriber = this.session.subscribe(
          event.stream,
          undefined
        );
        this.subscribers.push(subscriber); // subscribers created here
      });

      if (!this.token) {
        this.token = data.token;
        this.session.connect(this.token).then(() => {
          if (this.platform.is("cordova")) {
            console.log("in connect cordova==>>");
            if (this.platform.is("android")) {
              console.log("Android platform=>>>>");
              this.checkAndroidPermissions()
                .then(() => this.initPublisher())
                .catch((err) => console.error(err));
            }
          } else {
            this.initPublisher();
          }
        });
      }
    });
    this.socket.on("userRegistered", (data) => {
      console.log("userRegistered/*****", data);
      this.refid = data.refid;
    });

    this.socket.on("userLeft", (data) => {
      console.log("userLeft//******", data);
      if (this.users[data.emailid]) delete this.users[data.emailid];
    });
  }

  initPublisher() {
    console.log("in init publisher/********");
    const publisher: Publisher = this.OV.initPublisher(undefined, {
      audioSource: undefined,
      videoSource: undefined,
      publishAudio: true,
      publishVideo: true,
      resolution: "640x480",
      frameRate: 30,
      insertMode: "APPEND",
      mirror: true,
    });
    this.session.publish(publisher).then(() => {
      this.publisher = publisher; // publisher created here
    });
  }

  toggleVideo() {
    if (this.publisher) {
      this.videoMute = true;
      this.publisherView = this.publisher;
      this.publishVideo = !this.publishVideo;
      this.publisherView.publishVideo(this.publishVideo);
      this.publisher = this.publisherView;
    }
  }

  toggleAudioMute() {
    if (this.publisher) {
      this.audioMute = true;
      this.publisherView = this.publisher;
      this.publishAudio = !this.publishAudio;
      this.publisherView.publishAudio(this.publishAudio);
      this.publisher = this.publisherView;
    }
  }
  toggleAudioUnmute() {
    if (this.publisher) {
      this.audioMute = false;
      this.publisherView = this.publisher;
      this.publishAudio = !this.publishAudio;
      this.publisherView.publishAudio(this.publishAudio);
      this.publisher = this.publisherView;
    }
  }
  shareScreen() {
    this.OV.getUserMedia({ videoSource: "screen" }).then((mediaStream) => {
      console.log("****mediastream////", mediaStream);
      this.publisher.updateMediaStream(mediaStream.getVideoTracks[0]);
    });
  }
  Logout() {
    if (this.session) {
      this.session.disconnect();
    }
    this.socket.emit("endCall", {
      emailid: this.currentUser,
      refid: this.refid,
    });
    localStorage.removeItem("userId");
    this.otherUser = null;
    this.users = [];
    this.currentUser = null;
    location.reload();
  }
}
