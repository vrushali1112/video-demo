import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { StreamManager, StreamPropertyChangedEvent } from 'openvidu-browser';
import { Platform } from '@ionic/angular';
declare var cordova;

@Component({
    selector: 'ov-video',
    template: '<video #videoElement style="width: 100%"></video>'
})
export class VideoComponent implements AfterViewInit {

    @ViewChild('videoElement') elementRef: ElementRef;
    _streamManager: StreamManager;

    constructor(private platform: Platform) {}

    ngAfterViewInit() {
        this.updateVideoView();
    }

    @Input()
    set streamManager(streamManager: StreamManager) {
        this._streamManager = streamManager;
    }

    private updateVideoView() {
        this._streamManager.addVideoElement(this.elementRef.nativeElement);
    }
}